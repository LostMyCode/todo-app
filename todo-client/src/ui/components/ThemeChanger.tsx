import React from "react";
import { useSelector, useDispatch } from "react-redux";
import styled from "styled-components";
import { Toggle } from "@pancakeswap-libs/uikit";
import { changeTheme } from "../../utils/Reducer";

const Wrap = styled.div`
  position: absolute;
  top: 30px;
  right: 30px;
`;

const ThemeChanger: React.FC<{}> = ({ children }) => {
  const isDark: boolean = useSelector((state: any) => state.isDark);
  const dispatch = useDispatch();

  const onToggleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(changeTheme(e.target.value));
  }

  return (
    <Wrap>
      <Toggle
        scale="sm"
        checked={isDark}
        onChange={(e) => onToggleChange(e)}
      />
    </Wrap>
  );
};

export default ThemeChanger;
