import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Radio, Flex, Text } from "@pancakeswap-libs/uikit";

interface ListItemProps {
  name: string;
  value: string | number;
  isChecked?: boolean;
  onChange: (val: any) => void;
  disabled?: boolean;
}

const Label = styled.label<{ isDisabled: boolean | undefined }>`
  cursor: ${({ isDisabled }) => (isDisabled ? "not-allowed" : "pointer")};
  display: flex;
  justify-content: space-between;
  align-items: center;
  opacity: ${({ isDisabled }) => (isDisabled ? "0.6" : "1")};
  margin-bottom: 0 !important;
`;

const Body = styled.div`
  align-items: center;
  border-top: 2px solid ${({ theme }) => theme.colors.tertiary};
  //   border-radius: 16px 0 0 16px;
  display: flex;
  flex-grow: 1;
  height: 80px;
  padding: 8px 16px;
`;

const Children = styled.div`
  margin-left: 20px;
`;

const ListItem: React.FC<ListItemProps> = ({
  name,
  value,
  isChecked,
  onChange,
  children,
  ...props
}) => {
  const [checked, setChecked] = useState(isChecked);
  const handleChange = (e: any) => {
    setChecked(true);
    setTimeout(() => {
      onChange(e.target.value);
    }, 500);
  };

  return (
    <Label isDisabled={false}>
      <Body>
        <Radio
          name={name}
          checked={checked}
          value={value}
          onChange={(e) => handleChange(e)}
          disabled={false}
          style={{ flex: "none", outline: "none" }}
        />
        <Children>{children}</Children>
      </Body>
    </Label>
  );
};

export default ListItem;
