import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import {
  updateTodo as updateTodoStore,
  addDone as addDoneStore,
  updateDone as updateDoneStore,
} from "../../utils/Reducer";
import {
  ButtonMenu,
  ButtonMenuItem,
  Text,
  Card,
  CardBody,
} from "@pancakeswap-libs/uikit";
import * as FirestoreHandler from "../../utils/FirestoreHandler";
import ListItem from "./ListItem";
import AppLogo from "../../assets/img/todo-app-logo.png";
import ThemeChanger from "./ThemeChanger";

const GradCard = styled.div`
  background: linear-gradient(270deg, #8967f9, #4f70ff);
  border-radius: 32px;
  position: relative;
`;

const StyledCard = styled(Card)`
  position: absolute;
  width: 100%;
  top: 143px;
  bottom: 0;
`;

const ListContainer = styled.div`
  position: absolute;
  padding: 20px;
  top: 80px;
  bottom: 100px;
  left: 10px;
  right: 20px;
  overflow-y: scroll;
`;

let c = 0;

const TodoList: React.FC = () => {
  const dispatch = useDispatch();
  const [mode, setMode] = useState(0); // 0 = todo, 1 = done
  const handleClick1 = (newIndex: number) => setMode(newIndex);
  const todoItems: string[] = useSelector((state: any) => state.todoItems);
  const doneItems: string[] = useSelector((state: any) => state.doneItems);

  const onTodoDone = (index: number) => {
    const currentDoneItem = todoItems[index];
    const filtered = todoItems.filter((a, b) => b !== index);
    FirestoreHandler.updateList(filtered, "todo");
    FirestoreHandler.updateList([...doneItems, currentDoneItem], "done");
    dispatch(updateTodoStore(filtered));
    dispatch(updateDoneStore([...doneItems, currentDoneItem]));
  };

  const getListByMode = () => {
    return mode === 0 ? todoItems : doneItems;
  };

  useEffect(() => {
    (async () => {
      const data = await FirestoreHandler.getTodoAndDone();
      dispatch(updateDoneStore(data.done));
      dispatch(updateTodoStore(data.todo));
    })();
  }, []);

  return (
    <GradCard className="h100">
      <div style={{ padding: "20px 0" }}>
        <img src={AppLogo} alt="todo-app-logo" />
      </div>
      <StyledCard>
        <CardBody>
          <div style={{ marginBottom: "20px" }}>
            <ButtonMenu
              activeIndex={mode}
              onItemClick={handleClick1}
              scale="sm"
              variant="subtle"
            >
              <ButtonMenuItem>TODO</ButtonMenuItem>
              <ButtonMenuItem>DONE</ButtonMenuItem>
            </ButtonMenu>
          </div>
          <ThemeChanger />
          <ListContainer>
            {getListByMode().length === 0 ? (
              <div style={{ marginTop: "30px" }}>TODOリストが空です</div>
            ) : (
              getListByMode().map((v, i) => {
                c++;
                return (
                  <ListItem
                    key={"todo-item" + c}
                    name={"item" + i}
                    value={i}
                    onChange={(a) => {
                      onTodoDone(i);
                    }}
                    isChecked={mode === 1}
                    disabled={mode === 1}
                  >
                    <Text>{v}</Text>
                  </ListItem>
                );
              })
            )}
          </ListContainer>
        </CardBody>
      </StyledCard>
    </GradCard>
  );
};

export default TodoList;
