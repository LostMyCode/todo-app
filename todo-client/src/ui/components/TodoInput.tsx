import React from "react";
import styled from "styled-components";
import { Input } from "@pancakeswap-libs/uikit";
import { useSelector, useDispatch } from "react-redux";
import { updateTodo as updateTodoStore } from "../../utils/Reducer";
import * as FirestoreHandler from "../../utils/FirestoreHandler";

const Button = styled.div`
  border-radius: 50px;
  width: 70px;
  height: 70px;
  line-height: 70px;
  color: #fff;
  font-weight: bold;
  font-size: 30px;
  background: linear-gradient(200deg, #8967f9, #4f70ff);
  position: absolute;
`;

const Adder = styled.div`
  position: absolute;
  background-color: /* #483f5a */${({ theme }) => theme.colors.input};
  border-radius: 50px;
  bottom: 0;
  width: 100%;
`;

const AddTodoButton: React.FC = () => {
  const storedTodoItems: string[] = useSelector(
    (state: any) => state.todoItems
  );
  const dispatch = useDispatch();
  const onNewTodoEntry = (e: any) => {
    console.log(e.target.value);
    if (e.target.value) {
      const newList = [...storedTodoItems, e.target.value];
      dispatch(updateTodoStore(newList));
      FirestoreHandler.updateList(newList, "todo");
      e.target.value = "";
    }
  };

  return (
    <Adder>
      <Button>+</Button>
      <Input
        id="todo-input"
        type="text"
        scale="lg"
        placeholder="TODOを入力"
        style={{
          height: "70px",
          borderRadius: "50px",
          marginLeft: "70px",
          width: "calc(100% - 70px)",
        }}
        onKeyPress={(e: any) => {
          if (e.key === "Enter") {
            onNewTodoEntry(e);
          }
        }}
      ></Input>
    </Adder>
  );
};

export default AddTodoButton;
