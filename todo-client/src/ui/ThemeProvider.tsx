import React from "react";
import { useSelector } from "react-redux";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import { light, dark, ResetCSS } from "@pancakeswap-libs/uikit";
import { PancakeTheme } from "@pancakeswap-libs/uikit/dist/theme";

declare module "styled-components" {
  /* eslint-disable @typescript-eslint/no-empty-interface */
  export interface DefaultTheme extends PancakeTheme {}
}

const GlobalStyle = createGlobalStyle`
  * {
    font-family: 'Kanit', sans-serif;
  }
  
  html {
    height: 100%;
  }

  body {
    height: 100%;
    padding: 20px;
    background-color: ${({ theme }) => theme.colors.background};
    img {
      height: auto;
      max-width: 100%;
    }
  }
`;

const CakeThemeProvider: React.FC<{}> = ({ children }) => {
  const isDark: boolean = useSelector((state: any) => state.isDark);
  return (
    <ThemeProvider theme={isDark ? dark : light}>
      <ResetCSS />
      <GlobalStyle />
      {children}
    </ThemeProvider>
  );
};

export default CakeThemeProvider;
