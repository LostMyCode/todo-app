import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  todoItems: [],
  doneItems: [],
  isDark: ((localStorage.getItem("todoapp_theme") || "dark") === "dark" ? true : false),
};

const slice = createSlice({
  name: "todo",
  initialState,
  reducers: {
    updateTodo: (state, action) => {
      return Object.assign({}, state, { todoItems: action.payload });
    },
    addDone: (state, action) => {
      return Object.assign({}, state, {
        doneItems: [...state.doneItems, action.payload],
      });
    },
    updateDone: (state, action) => {
      return Object.assign({}, state, { doneItems: action.payload });
    },
    changeTheme: (state, action) => {
      localStorage.setItem("todoapp_theme", (!state.isDark) ? "dark" : "white");
      return Object.assign({}, state, { isDark: !state.isDark });
    },
  },
});

export default slice.reducer;
export const { updateTodo, addDone, updateDone, changeTheme } = slice.actions;
