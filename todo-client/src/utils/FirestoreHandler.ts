import firebase from "firebase";

const db = firebase.firestore();

const updateList = async (updatedList: string[], target: string) => {
  const todos = await db.collection("todoapp").doc(target);
  todos.update({ tasks: updatedList });
};

const getList = async (target: string) => {
  const todos = await db.collection("todoapp").doc(target).get();
  const todosData: any = todos.data();
  return todosData.tasks;
};

const getTodoAndDone = async () => {
  const todo = await getList("todo");
  const done = await getList("done");
  return {
    todo,
    done,
  };
};

export { updateList, getList, getTodoAndDone };
