import React from "react";
import "./App.css";
import "./utils/firebase";
import { Provider } from "react-redux";
import store from "./utils/Store";

import ThemeProviderWrap from "./ui/ThemeProvider";
import TodoList from "./ui/components/TodoList";
import TodoInput from "./ui/components/TodoInput";

function App() {
  return (
    <Provider store={store}>
      <ThemeProviderWrap>
        <TodoList />
        <TodoInput />
      </ThemeProviderWrap>
    </Provider>
  );
}

export default App;
