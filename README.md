# todo-app

シンプルなTODOアプリ(web) 選考用

[制作物のデモはこちら](https://august-storm-267205.web.app/)

## スクリーンショット

![ss](https://cdn.discordapp.com/attachments/784748270902706176/825357279633211443/unknown.png)

ダークモードで表示した場合

## 使用技術や環境

- React
- TypeScript
- Redux
- Pancake UI kit
- Firebase (Firestore)

## 開発用

インストール

```
yarn install
```

クライアント側の開発

```
cd todo-client
yarn start
```

ビルド

```
cd todo-client
yarn build
```

本番環境デプロイ

```
firebase deploy
```